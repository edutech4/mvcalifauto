from flask_wtf import FlaskForm
from wtforms import StringField, SelectMultipleField, PasswordField, SubmitField

class userForm(FlaskForm):
    nombres = StringField('Nombres')
    apellidos = StringField('Apellidos')
    email = StringField('email')
    usertype = StringField('usertype')
    password = PasswordField('contraseña')                                                          