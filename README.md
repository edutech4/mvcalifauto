# mvcalifauto

Aplicación desarrollo de sistema de calificación automatica.

Se realiza conexión a la base de datos mysql

# Instalación de paquetes

python3 -m venv env   ## Se crea el entorno virtual con 

pip install flask

pip install flask_sqlalchemy  ## Se instala dependencia para que Flask conecte con DB

pip install flask-mysql-connector  ##  Conector mysql flask

pip install flask_wtf ## herramienta para formularios
