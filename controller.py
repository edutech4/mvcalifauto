from app import app
from models import *
from flask import render_template
from forms import userForm


with app.app_context():                                         
    db.create_all()
    db.session.commit()

@app.route('/')
def index():
    return render_template ('index.html')

@app.route('/nuevo_usuario', methods=['GET', 'POST'])
def nuevo_usuario():
    form = userForm() 
    if form.validate_on_submit():
        nombres = form.nombres.data
        apellidos = form.apellidos.data
        email = form.email.data
        usertype = form.usertype.data
        password = form.password.data
        
        #pais = new_func(form)

        nuevo_usuario = usuarios(nombres=nombres, apellidos=apellidos, email=email, usertype=usertype, password=password)

        with app.app_context():
            db.session.add(nuevo_usuario)
            db.session.commit()

        return 'Equipo registrado exitosamente'
    
    return render_template('nuevo_usuario.html', form=form)

#def new_func(form):
    pais = form.pais.data
    return pais